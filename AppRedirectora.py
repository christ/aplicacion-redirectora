import socket
import random

urls = [   'https://www.google.com',
    'https://www.facebook.com',
    'https://www.amazon.com',
    'https://www.microsoft.com',
    'https://www.apple.com'
]

host = 'localhost'
port = 1235


def answer_url():
    url_random = random.choice(urls)
    answer = "HTTP/1.1 307 Temporary Redirect  \r\n" \
             + "Location:" + url_random + "\r\n\r\n"
    return answer


Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
Socket.bind((host, port))


Socket.listen(5)


try:
    print("Serving at port", port)
    while True:
        print("Waiting for connections...")
        (recvSocket, address) = Socket.accept()
        print(address, "is already connected")
        print("Request received:")
        print(recvSocket.recv(2048))
        print("Answering back...")
        recvSocket.send(answer_url().encode('ascii'))
        recvSocket.close()
except KeyboardInterrupt:
    print("The server was closed due to a Keyboard Interrupt")
    Socket.close()

